<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Upload Facebook Offline Event</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    
    <style type="text/css">
        html,
        body {
          height: 100%;
        }

        .content-wrap {
          height: 100%;
          display: -ms-flexbox;
          display: flex;
          -ms-flex-align: center;
          align-items: center;
          padding-top: 40px;
          padding-bottom: 40px;
          background-color: #f5f5f5;
        }

        .form-wrap {
          width: 100%;
          max-width: 400px;
          padding: 15px;
          margin: auto;
        }
        .form-data .checkbox {
          font-weight: 400;
        }
        .form-data .form-control {
          position: relative;
          box-sizing: border-box;
          height: auto;
          padding: 10px;
          font-size: 16px;
        }
        .form-data .form-control:focus {
          z-index: 2;
        }
        .form-data input[type="text"] {
          margin-bottom: -1px;
          border-bottom-right-radius: 0;
          border-bottom-left-radius: 0;
        }
        .form-data input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
.gj-datepicker {
    width: 100% !important;
}
.gj-datepicker .input-group-append button {
    height: 46px;
}
.gj-datepicker-bootstrap [role=right-icon] button .gj-icon {
    top: 13px;
}        
.img-loading {
    text-align: center;
    display: none;
}
.form-submitting .img-loading {
  display: block !important;
}
.form-submitting form {
  display: none !important;
}
.generating {
  margin-top: 10px;
}
@keyframes blink {
    /**
     * At the start of the animation the dot
     * has an opacity of .2
     */
    0% {
      opacity: .2;
    }
    /**
     * At 20% the dot is fully visible and
     * then fades out slowly
     */
    20% {
      opacity: 1;
    }
    /**
     * Until it reaches an opacity of .2 and
     * the animation can start again
     */
    100% {
      opacity: .2;
    }
}

.generating span {
    /**
     * Use the blink animation, which is defined above
     */
    animation-name: blink;
    /**
     * The animation should take 1.4 seconds
     */
    animation-duration: 1.4s;
    /**
     * It will repeat itself forever
     */
    animation-iteration-count: infinite;
    /**
     * This makes sure that the starting style (opacity: .2)
     * of the animation is applied before the animation starts.
     * Otherwise we would see a short flash or would have
     * to set the default styling of the dots to the same
     * as the animation. Same applies for the ending styles.
     */
    animation-fill-mode: both;
}

.generating span:nth-child(2) {
    /**
     * Starts the animation of the third dot
     * with a delay of .2s, otherwise all dots
     * would animate at the same time
     */
    animation-delay: .2s;
}

.generating span:nth-child(3) {
    /**
     * Starts the animation of the third dot
     * with a delay of .4s, otherwise all dots
     * would animate at the same time
     */
    animation-delay: .4s;
}
    </style>

  </head>

  <body>

	<div class="content-wrap">
		<div class="form-wrap">
		<form action="FormatOfflineEvent.php" id="form" method="post" class="form-data" enctype="multipart/form-data">
			<div class="form-group">
				<label for="event_set_id">Event Set ID</label>
				<input type="text" class="form-control" required="required" id="event_set_id">
			</div>
			<div class="form-group">
				<label for="fullfilment">Fulfilment Report</label>
				<input type="file" class="form-control-file" name="fullfilment" required="required" id="fullfilment"  aria-describedby="fileHelp">
				<small id="fileHelp" class="form-text text-muted">File must be xls or csv.</small>
        <em id="fullfilment-msg" class="msg small" style="color:red;"></em>
			</div>
      <div class="form-group">
        <label for="sales">Sales Summary Report</label>
        <input type="file" class="form-control-file" name="sales" required="required" id="sales"  aria-describedby="fileHelp">
        <small id="fileHelp" class="form-text text-muted">File must be xls or csv.</small>
        <em id="sales-msg" class="msg small" style="color:red;"></em>
      </div>
			<button type="submit" class="btn btn-primary">Upload</button>
		</form>
		<div class="img-loading">
			<p class="generating">Uploading<span>.</span><span>.</span><span>.</span></p>
		</div>
		</div>
	</div>

<script type="text/javascript">
	
	$('#form').submit(function(e) {

		e.preventDefault();

	    var fullfilment = $('#fullfilment').prop('files')[0];
      var sales = $('#sales').prop('files')[0];
	    var form_data = new FormData();

	    form_data.append('fullfilment', fullfilment);
      form_data.append('sales', sales);
	    
	    $.ajax({
	        url: 'FormatOfflineEvent.php', 
	        dataType: 'json',
	        cache: false,
	        contentType: false,
	        processData: false,
	        data: form_data,                         
	        type: 'post',
	        beforeSend: function() {
              $('.form-wrap').addClass('form-submitting');
              $('#fullfilment-msg, #sales-msg').text('');
          },
	        success: function(data){	 

            if( data.status == 'error' ) {
              $.each(data.message, function(key, value) {
                console.log(key);
                console.log(value);                
                $('#'+key+'-msg').text(value);
              });
              $('.form-wrap').removeClass('form-submitting');
            } else {
              toFB(data);
            }	          
	        }
	    });
	});

	function toFB( dataArray ) {
		
		var eventID = $('#event_set_id').val();
		var apiURL = 'https://graph.facebook.com/v3.0/'+ eventID +'/events';
				
		$.ajax({
	        url: apiURL, 
	        cache: false,
	        data: {
	        	access_token: "EAAFw1OiHiLQBAKGd0d8MO4OHNSkkiTJCDacfZBuCMLgCnEcbzyucBrZArbGxQgCKXMTXZBnhSjUUcfSeTwrW8s5VbGhVirbakOpi0aQ0gApFWZBNQZAJVILwsqJNtpiUOnkT03EcZAuiwPZB1rqbgI2QTmbcZCWIIAjNQNLrgsZAMZBXQ7ZCPawn0eR",
	        	upload_tag: "orders",
	        	data: dataArray
	        },                         
	        type: 'post',
	        success: function(result){	            
	            $('.generating').html('Successfully uploaded!');
	        }
	    });
	}

</script>

</body>
</html>