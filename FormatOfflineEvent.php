<?php

include 'PHPExcel-develop/Classes/PHPExcel.php';
include 'simplexlsx.class.php';

class FormatOfflineEvent
{
	private $data;
	private $mergeData = false;
	private $uniqueColumn = false;
	private $token;
	private $uploadTag;
	private $eventSetID;	
	private $apiVars = [
				'url' 	  => 'https://graph.facebook.com/',
				'version' => 'v2.10',
				'endpoint' => 'events'
			];
	private $renameHeaders = [];
	private $insertColumns = [];
	public $allowExtension = ['xls', 'xlsx', 'csv'];

	public function data( $file )
	{
		$this->mainData = $this->getFileData( $file );
		return $this;
	}

	public function merge( $file, $match = false, $column = false )
	{
		$this->mergeData[] = [
			'file' 	 => $file,
			'match'  => $match,
			'column' => $column,
			'data'   => $this->getFileData( $file )
		];

		return $this;
	}

	public function doMergeData()
	{
		foreach ($this->mergeData as $data) {

			$matches = [];
			$mergeHeaders = $data['data'][0];
			$mergeCol_ID  = array_search($data['match'][1], $mergeHeaders);
			$mergeVal_ID  = array_search($data['column'][0], $mergeHeaders);

			unset($data['data'][0]);

			foreach ($data['data'] as $match) {
				$matches[$match[3]] = $match[17];				
			}
			
			$merge = [$data['match'][0] => $matches];					
		}		

		$dataHeaders = $this->getHeaders();
		$dataRows 	 = $this->getRows();
		$dataCol_ID  = array_search($data['match'][0], $dataHeaders);
		$dataVal_ID  = array_search($data['column'][1], $dataHeaders);

		$newData  = [];	
		foreach ($dataRows as $dKey => $row) {
			foreach ($row as $rKey => $field) {
				if( $rKey == $dataCol_ID ) {
					if( isset($merge[$data['match'][0]][$field]) ) {
						$value = str_replace('$', '', $merge[$data['match'][0]][$field]);
						$value = (float) $value;						
						if( $value > 0 ) {
							$dataRows[$dKey][$dataVal_ID] = (float) $value;	
						} else {
							unset($dataRows[$dKey]);
						}						
					} else {						
						unset($dataRows[$dKey]);
					}					
				}
			}			
		}

		array_unshift($dataRows, $dataHeaders);
		$this->mainData = $dataRows;			
	}

	public function token( $token )
	{
		$this->token = $token;
		return $this;
	}

	public function uploadTag( $tag )
	{
		$this->uploadTag = $tag;
		return $this;
	}

	public function eventSetID( $id )
	{
		$this->eventSetID = $id;
		return $this;
	}

	public function renameHeaders($replace)
	{
		$this->renameHeaders = $replace;
		return $this;
	}

	public function doRenameHeaders()
	{
		foreach ($this->renameHeaders as $header) {
			if( isset($header[1]) ) {
				foreach ($this->mainData[0] as $key => $name) {
					if( $header[0] == $name ) {
						$this->mainData[0][$key] = $header[1];
					}
				}		
			}		
		}
	}

	public function insertColumns( $cols )
	{
		$this->insertColumns = $cols;
		return $this;
	}

	public function doInsertColumns()
	{
		$rows 	= $this->getRows();
		$header = $this->getHeaders();

		foreach ($this->insertColumns as $val) {
			$header[] = $val[0];
		}

		if( !empty($rows) ) {
			foreach ($rows as $key => $row) {
				foreach ($this->insertColumns as $newVal) {
					$rows[$key][] = $newVal[1];
				}
			}
		}		

		array_unshift($rows, $header);

		$this->mainData = $rows;
	}

	public function getFileData( $file )
	{
		$ext = $this->validateFileExtension($file['name']);

		$fileName = $file['tmp_name'];

		if( $ext != 'csv' ) {
			$output = 'output.csv';
			$this->convertXLStoCSV($file['tmp_name'], $output);
			$fileName = $output;
		}

		$data = array_map('str_getcsv', file($fileName));

		return $data;
	}

	public function validateFileExtension( $file )
	{
		$ext   = pathinfo($file);
		$ext   = $ext['extension'];		 

		if( !in_array($ext, $this->allowExtension) ) { 
			echo json_encode([
				'status'  => 'error',
				'message' => 'Invalid file type'
			]); 			
			die();
		}

		return $ext;
	}

	public function convertXLStoCSV( $infile, $outfile )
	{
	    $fileType  = PHPExcel_IOFactory::identify($infile);
	    $objReader = PHPExcel_IOFactory::createReader($fileType);
	 
	    $objReader->setReadDataOnly(true);   
	    $objPHPExcel = $objReader->load($infile);    
	 
	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
	    $objWriter->save($outfile);
	}

	public function build()
	{
		$this->doRenameHeaders();
		$this->doInsertColumns();

		if( $this->uniqueColumn ) {
			$this->removeDuplicates();
		}	

		if( $this->mergeData ) {
			$this->doMergeData();
		}	

		$data = $this->parseData();

		return json_encode($data);
	}

	public function parseData()
	{	
		$parsed = [];

		foreach ($this->getRowsAssociative() as $row) {
			$parsed[] = $this->formatRow($row);
		}

		return $parsed;
	}

	public function setUniqueColumn( $unique )
	{
		$this->uniqueColumn = $unique;
		return $this;
	}

	public function removeDuplicates()
	{
		$headers = $this->getHeaders();
		$rows 	 = $this->getRows();
		$colID   = array_search( $this->uniqueColumn, $headers );

		$order_ids = [];
		$newData   = [];

		foreach ($rows as $row) {
			$not_duplicate = true;
			foreach ($row as $key => $field) {
				if( $key == $colID ) {
					if( in_array($field, $order_ids) ) {
						$not_duplicate = false;
					}
					$order_ids[] = $field;
				}				
			}

			if( $not_duplicate ) {
				$newData[] = $row;
			}
		}

		array_unshift($newData, $headers);
		
		$this->mainData = $newData;
	}

	public function formatDate($str)
	{
		$str = str_replace('-', '/', $str);
		$str = explode('/', $str);		
		$str = $str[1] .'/'. $str[0] .'/'. $str[2];
		
		$date 	 = date_create($str);
		$newDate = date_format($date, "Y-m-d H:i:s");
		$newDate = strtotime($newDate);

		return $newDate;		    
	}

	public function formatRow( $item )
	{
		$newData  = [];
		$parameters = [
			'event_name',
			'event_time',
			'order_id',
			'value',
			'currency'
		];

		$item['event_time'] = $this->formatDate($item['event_time']);

		foreach ($parameters as $param) {			
			$newData[$param] = $item[$param];
			unset($item[$param]);
		}

		$newData['match_keys'] = [];
		$newData['custom_data'] = [];

		foreach ($item as $key => $field) {
			$mKeys = $this->matchKeys();
			if( in_array($key, $mKeys) ) {
				$newData['match_keys'][$key] = hash('sha256', $field);
				unset($item[$key]);
			}
		}

		foreach ($item as $key => $field) {
			$newData['custom_data'][$key] = $field;
		}

		return $newData;
	}

	public function getHeaders()
	{
		return $this->mainData[0];
	}

	public function getRows()
	{
		$rows = $this->mainData;
		unset($rows[0]);

		return $rows;
	}

	public function getRowsAssociative()
	{
		$rows = [];
		$headers = $this->getHeaders();

		foreach ($this->getRows() as $value) {
			$items = [];

			foreach ($value as $key => $item) {
				$hKey = $this->strSlug($headers[$key]);
				$items[$hKey] = $item;
			}
			$rows[] = $items;
		}

		return $rows;
	}

	public function strSlug( $str )
	{
		$str = str_replace(' ', '_', $str);
		return strtolower($str);
	}

	// Match Keys | https://developers.facebook.com/docs/marketing-api/offline-conversions
	public function matchKeys()
	{
		$matchKeys = [
			'email',
			'phone',
			'gen',
			'doby',
			'dobm',
			'dobd',
			'ln',
			'fn',
			'fi',
			'ct',
			'st',
			'zip',
			'country',
			'madid',
			'madid',
			'extern_id',
			'lead_id'
		];

		$headerMKeys = [];

		foreach ($this->getHeaders() as $key => $val) {
			if( in_array($val, $matchKeys) ) {
				$headerMKeys[$key] = $val;
			}
		}

		return $headerMKeys;
	}

} // end class

$allowFiles = ['xls', 'csv'];

$fullfilment = isset($_FILES['fullfilment'])? $_FILES['fullfilment'] : false;
$salesReport = isset($_FILES['sales'])? $_FILES['sales'] : false;

$errors = [];

if( strpos($fullfilment['name'], 'FulfillmentReport') === false ) {
	$errors['fullfilment'] = 'Invalid Fulfilment Report File.';
}

if( strpos($salesReport['name'], 'SalesSummarySales') === false ) {
	$errors['sales'] = 'Invalid Sales Summary Report File.';
}

$fullfilment_ext = pathinfo($fullfilment['name']);
$fullfilment_ext = $fullfilment_ext['extension'];

$sales_ext = pathinfo($salesReport['name']);
$sales_ext = $sales_ext['extension'];

if( !in_array($fullfilment_ext, $allowFiles) ) {
	$errors['fullfilment'] = 'Invalid File Type.';
}

if( !in_array($sales_ext, $allowFiles) ) {
	$errors['sales'] = 'Invalid File Type.';
}

if( !empty($errors) ) {
	echo json_encode([
		'status' => 'error',
		'message' => $errors
	]);
	die();
}

// Envoke class
$data 	= new FormatOfflineEvent();
$result = $data->data($fullfilment) 				// main data to be uploaded
				->merge(			 				// merge new data method
					$salesReport, 		 			// secondary file data to merge
					['order_id', 'Order No'], 		// column names to match between two data
					['Sales Inc', 'value']			// [0] column name from secondary data to merge to main data | [1] where the data will be placed
				)
				->setUniqueColumn('order_id')
				->renameHeaders([
					['DateCreated', 'event_time'],
					['OrderNumber', 'order_id'],
					['Email', 'email'],
					['Phone', 'phone'],
					['State', 'st'],
					['Postcode', 'zip']
				])
			   	->insertColumns([
			   		['event_name', 'Purchase'],			   		
			   		['currency', 'AUD'],
			   		['value', 'value here']
			   	])
			   	->build();
echo $result;